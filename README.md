# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do modemu GSM Quectel M95.

- Zasilanie bezpośrednie +3,3 ... 4,6 V
- Gniazdo karty u-SIM
- Gniazdo Jack 3,5 mm 4-pin do headset
- Gniazdo SMA do anteny zewnętrznej
- LED *Network*
- Wyprowadzony full-UART do rozkazów AT
  - Sygnały UART są buforowane przez translator poziomów TXB0108PW + LDO 3,3 V
- Opcjonalne dwa złącza szpilkowe (headers) do płytek/przewodów stykowych
  - 22 piny
  - Raster 2,54 mm
  - Rozsunięte na 39,0 mm (pasuje do [Wish Board WB-104-1](https://www.tme.eu/pl/details/wb-104-1/plytki-uniwersalne/wisher-enterprise/))

# Projekt PCB

Schemat: [doc/GSM_M95_EVB_V1_0_SCH.pdf](doc/GSM_M95_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/GSM_M95_EVB_V1_0_3D.pdf](doc/GSM_M95_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

| Top layer | Top assembly | 3D preview |
|-----------|--------------|------------|
| ![](doc/foto/PCB-altium-top.png "Top layer")  | ![](doc/foto/PCB-altium-assembly.png "Top assembly") | ![](doc/foto/PCB-altium-3D.png "3D preview") |

# Licencja

MIT